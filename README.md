# Blackboki

Creación de una aplicación con BlackBerry JDE
Complemento para Eclipse

Nos centraremos en el código, no en el entorno de desarrollo, pero los detalles sobre la creación del proyecto, la adición de clases y archivos de recursos, y la ejecución y la depuración serán diferentes. Nuestra aplicación es una aplicación simple de BlackBerry con una sola pantalla que mostrará "Hello World". Pasaremos por la creación del espacio de trabajo y el proyecto, la creación y construcción de las clases necesarias y la compilación y ejecución en un simulador. Luego, agregaremos algunos detalles adicionales, incluido un nombre de aplicación adecuado, información de la versión y un ícono. Finalmente, verá cómo compilar, firmar y ejecutar la aplicación en un dispositivo real. [192.168.l00.1](https://www.stockbitcoin.info/ip/192-168-100-1/)

Este es un inicio rápido, por lo que el objetivo es obtener una competencia básica con las herramientas de desarrollo y un poco de comprensión de los conceptos básicos de una aplicación BlackBerry sin profundizar demasiado. Probablemente tendrá muchas preguntas a lo largo de este capítulo, pero la mejor manera de proceder es ir paso a paso; las cosas se aclararán a medida que exploremos los detalles del desarrollo de aplicaciones más adelante.

Hay mucho que hacer aquí, así que vamos.

## Creación de una aplicación con BlackBerry JDE

Complemento para Eclipse

Si ha trabajado con Eclipse en el pasado en un proyecto de Java genérico, la creación de un proyecto de BlackBerry será muy similar a lo que ha hecho con otros proyectos de Java genéricos.
Veremos cómo crear la misma aplicación Hello World con el complemento JDE para Eclipse. Junto con el tutorial, también proporcionaremos explicaciones del código para brindarle una imagen más completa.
Si no ha instalado el complemento JDE para Eclipse, más adelante le indicamos algunas instrucciones sobre dónde obtenerlo y cómo instalarlo.

De forma predeterminada, se crearán dos clases, MyApp.java y MyScreen.java, en el paquete mypackage. Por el bien del tutorial, elimine el paquete para que pueda comenzar desde cero haciendo clic con el botón derecho en el paquete mypackage y seleccionando Eliminar.

## Crear las clases de aplicación

Crear archivos de clase es tan simple como crear un proyecto. Al crear nuevas clases, es una muy buena idea poner sus clases en paquetes. Como verá, Eclipse tiene una forma muy fácil de permitirle especificar el paquete para su clase.
Creación de la clase de aplicación principal.

Haga clic con el botón derecho en el icono del proyecto HelloWorld en el Explorador de paquetes y, en el menú emergente, seleccione Nueva clase. En el cuadro de diálogo, escriba los siguientes valores:

Package: com.beginningblackberry
Name: HelloWorldApp (puede omitir la extensión de archivo .java)
Superclass: net.rim.device.api.ui.UiApplication
